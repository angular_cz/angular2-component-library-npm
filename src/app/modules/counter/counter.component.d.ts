import { EventEmitter, OnInit } from '@angular/core';
export declare class CounterComponent implements OnInit {
    count: any;
    countChange: EventEmitter<number>;
    min: number;
    max: number;
    constructor();
    ngOnInit(): void;
    increase(): void;
    isMax(): boolean;
    decrease(): void;
    isMin(): boolean;
}
