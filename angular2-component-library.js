import { Component, EventEmitter, Input, NgModule, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

class CounterComponent {
    constructor() {
        this.countChange = new EventEmitter();
        this.min = 0;
        this.max = 100;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.count = this.count || this.min;
    }
    /**
     * @return {?}
     */
    increase() {
        if (this.isMax()) {
            return;
        }
        this.countChange.emit(this.count + 1);
    }
    /**
     * @return {?}
     */
    isMax() {
        return this.count >= this.max;
    }
    /**
     * @return {?}
     */
    decrease() {
        if (this.isMin()) {
            return;
        }
        this.countChange.emit(this.count - 1);
    }
    /**
     * @return {?}
     */
    isMin() {
        return this.count <= this.min;
    }
}
CounterComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-counter',
                template: `
    <div class="input-group">
      <span class="input-group-btn">
        <button class="btn btn-info" type="button" (click)="decrease()" [disabled]="isMin()">-</button>
      </span>
      <input type="text" class="form-control" readonly [value]="count">
      <span class="input-group-btn">
        <button class="btn btn-primary" type="button" (click)="increase()" [disabled]="isMax()">+</button>
      </span>
    </div>
  `,
                styles: [`

  `]
            },] },
];
/**
 * @nocollapse
 */
CounterComponent.ctorParameters = () => [];
CounterComponent.propDecorators = {
    'count': [{ type: Input },],
    'countChange': [{ type: Output },],
    'min': [{ type: Input },],
    'max': [{ type: Input },],
};

class CounterModule {
}
CounterModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [CounterComponent],
                exports: [CounterComponent]
            },] },
];
/**
 * @nocollapse
 */
CounterModule.ctorParameters = () => [];

/**
 * Generated bundle index. Do not edit.
 */

export { CounterModule, CounterComponent as ɵa };
//# sourceMappingURL=angular2-component-library.js.map
